%global _empty_manifest_terminate_build 0
Name:		python-futurist
Version:	3.0.0
Release:	1
Summary:	Useful additions to futures, from the future.
License:	Apache-2.0
URL:		https://docs.openstack.org/futurist/latest/
Source0:        https://files.pythonhosted.org/packages/4c/24/864408313afba48440ee3a560e1a70b62b39e6c0dfeddea9506699e6e606/futurist-3.0.0.tar.gz
BuildArch:	noarch

%description
Useful additions to futures, from the future.

%package -n python3-futurist
Summary:	Useful additions to futures, from the future.
Provides:	python-futurist
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-pbr
BuildRequires:	python3-pip
Requires:	python3-pbr
Requires:	python3-six
%description -n python3-futurist
Useful additions to futures, from the future.

%package help
Summary:	Development documents and examples for futurist
Provides:	python3-futurist-doc
%description help
Useful additions to futures, from the future. (docs)

%prep
%autosetup -n futurist-%{version}

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-futurist -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Fri Mar 29 2024 wangqiang <wangqiang1@kylinos.cn> - 3.0.0-1
- Update package to version 3.0.0

* Sun Jul 24 2022 liksh <liks11@chinaunicom.com.cn> - 2.4.1-1
- Upgrade for openstack Yoga

* Tue May 17 2022 liukuo <liukuo@kylinos.cn> - 2.3.0-3
- License compliance rectification

* Sun Aug 1 2021 huangtianhua <huangtianhua@huawei.com> - 2.3.0-2
- Adds pbr/pip as buildrequires

* Sat Nov 21 2020 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
